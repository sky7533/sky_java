package com.zhuorui.study;

import org.apache.commons.collections4.map.AbstractLinkedMap;

import java.util.Map;

public class SimpleLinkedHashMap<K,V> extends AbstractLinkedMap<K,V> {

    public SimpleLinkedHashMap() {
        super(DEFAULT_CAPACITY);
    }

    public SimpleLinkedHashMap(Map<? extends K, ? extends V> map) {
        super(map);
    }

    @Override
    public SimpleLinkEntry<K, V> getEntry(final Object key) {
        return (SimpleLinkEntry<K, V>) super.getEntry(key);
    }

    @Override
    protected LinkEntry<K, V> createEntry(final HashEntry<K, V> next, final int hashCode, final K key, final V value) {
        return new SimpleLinkEntry<K, V>(next, hashCode, convertKey(key), value);
    }

    public SimpleLinkEntry<K, V> getHeaderEntry(){
        return ((SimpleLinkEntry<K, V>) getEntry(0)).getBefore();
    }

    public static class SimpleLinkEntry<K, V> extends LinkEntry{

        /**
         * Constructs a new entry.
         *
         * @param next     the next entry in the hash bucket sequence
         * @param hashCode the hash code
         * @param key      the key
         * @param value    the value
         */
        public SimpleLinkEntry(HashEntry next, int hashCode, Object key, Object value) {
            super(next, hashCode, key, value);
        }

        public SimpleLinkEntry<K, V> getBefore(){
            return (SimpleLinkEntry<K, V>)super.before;
        }

        public SimpleLinkEntry<K, V> getAfter(){
            return ((SimpleLinkEntry<K, V>) super.after);
        }

    }

}
