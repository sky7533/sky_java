#### git bundle git离线团体合作
A和B不通过远程仓库同步代码，可以通过U盘，或发送文件的方式。
1. A和B都有项目的本地代码，（以前可以用远程仓库同步代码，现在不能用远程仓库同步代码）
A要把新commit 的内容同步到B
先获取B的最新的commit 的version number 假设是：9a466c5
在A的本地仓库中执行命令
```
git bundle create commits.bundle master ^9a466c5
```
会在A的仓库目录中生成一个文件 commits.bundle  master 当前本地的分支
把这个文件通过其他方式复制到B上和项目在同一个目录中，在B的本地仓库添加一个远程仓库：
```console
git remote add bundle ../commits.bundle
```
bundle 远程仓库的别名 随便取， ../commits.bundle 刚才复制的文件的相对项目的路径然后执行
```console
git pull bundle master:master
```
然后就可以把代码同步到B了
下次再同步只需要执行第一步，然后文件覆盖原来的commits.bundle文件，执行第三步就行了
2. B上一开始就没有本地仓库
   先在A上执行：
   ```console
   git bundle create repo.bundle HEAD master
   ```
把repo.bundle文件 复制到B上，执行：
```console
git clone repo.bundle repo
```
repo 是 克隆之后的仓库名 ，可以使用git remote -v 查看远程仓库的别名 和 url
后面增量更新了就可以用方式一

git pull origin master --allow-unrelated-histories 推送到已经存在的远程



